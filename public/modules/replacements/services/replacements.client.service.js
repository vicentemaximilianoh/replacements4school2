'use strict';

//Replacements service used to communicate Replacements REST endpoints
angular.module('replacements').factory('Replacements', ['$resource',
	function($resource) {
		return $resource('replacements/:replacementId', { replacementId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);