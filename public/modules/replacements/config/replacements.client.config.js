'use strict';

// Configuring the Articles module
angular.module('replacements').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Replacements', 'replacements', 'dropdown', '/replacements(/create)?');
		Menus.addSubMenuItem('topbar', 'replacements', 'List Replacements', 'replacements');
		Menus.addSubMenuItem('topbar', 'replacements', 'New Replacement', 'replacements/create');
	}
]);