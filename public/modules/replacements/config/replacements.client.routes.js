'use strict';

//Setting up route
angular.module('replacements').config(['$stateProvider',
	function($stateProvider) {
		// Replacements state routing
		$stateProvider.
		state('listReplacements', {
			url: '/replacements',
			templateUrl: 'modules/replacements/views/list-replacements.client.view.html'
		}).
		state('createReplacement', {
			url: '/replacements/create',
			templateUrl: 'modules/replacements/views/create-replacement.client.view.html'
		}).
		state('viewReplacement', {
			url: '/replacements/:replacementId',
			templateUrl: 'modules/replacements/views/view-replacement.client.view.html'
		}).
		state('editReplacement', {
			url: '/replacements/:replacementId/edit',
			templateUrl: 'modules/replacements/views/edit-replacement.client.view.html'
		});
	}
]);