'use strict';

(function() {
	// Replacements Controller Spec
	describe('Replacements Controller Tests', function() {
		// Initialize global variables
		var ReplacementsController,
		scope,
		$httpBackend,
		$stateParams,
		$location;

		// The $resource service augments the response object with methods for updating and deleting the resource.
		// If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
		// the responses exactly. To solve the problem, we define a new toEqualData Jasmine matcher.
		// When the toEqualData matcher compares two objects, it takes only object properties into
		// account and ignores methods.
		beforeEach(function() {
			jasmine.addMatchers({
				toEqualData: function(util, customEqualityTesters) {
					return {
						compare: function(actual, expected) {
							return {
								pass: angular.equals(actual, expected)
							};
						}
					};
				}
			});
		});

		// Then we can start by loading the main application module
		beforeEach(module(ApplicationConfiguration.applicationModuleName));

		// The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
		// This allows us to inject a service but then attach it to a variable
		// with the same name as the service.
		beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_) {
			// Set a new global scope
			scope = $rootScope.$new();

			// Point global variables to injected services
			$stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
			$location = _$location_;

			// Initialize the Replacements controller.
			ReplacementsController = $controller('ReplacementsController', {
				$scope: scope
			});
		}));

		it('$scope.find() should create an array with at least one Replacement object fetched from XHR', inject(function(Replacements) {
			// Create sample Replacement using the Replacements service
			var sampleReplacement = new Replacements({
				name: 'New Replacement'
			});

			// Create a sample Replacements array that includes the new Replacement
			var sampleReplacements = [sampleReplacement];

			// Set GET response
			$httpBackend.expectGET('replacements').respond(sampleReplacements);

			// Run controller functionality
			scope.find();
			$httpBackend.flush();

			// Test scope value
			expect(scope.replacements).toEqualData(sampleReplacements);
		}));

		it('$scope.findOne() should create an array with one Replacement object fetched from XHR using a replacementId URL parameter', inject(function(Replacements) {
			// Define a sample Replacement object
			var sampleReplacement = new Replacements({
				name: 'New Replacement'
			});

			// Set the URL parameter
			$stateParams.replacementId = '525a8422f6d0f87f0e407a33';

			// Set GET response
			$httpBackend.expectGET(/replacements\/([0-9a-fA-F]{24})$/).respond(sampleReplacement);

			// Run controller functionality
			scope.findOne();
			$httpBackend.flush();

			// Test scope value
			expect(scope.replacement).toEqualData(sampleReplacement);
		}));

		it('$scope.create() with valid form data should send a POST request with the form input values and then locate to new object URL', inject(function(Replacements) {
			// Create a sample Replacement object
			var sampleReplacementPostData = new Replacements({
				name: 'New Replacement'
			});

			// Create a sample Replacement response
			var sampleReplacementResponse = new Replacements({
				_id: '525cf20451979dea2c000001',
				name: 'New Replacement'
			});

			// Fixture mock form input values
			scope.name = 'New Replacement';

			// Set POST response
			$httpBackend.expectPOST('replacements', sampleReplacementPostData).respond(sampleReplacementResponse);

			// Run controller functionality
			scope.create();
			$httpBackend.flush();

			// Test form inputs are reset
			expect(scope.name).toEqual('');

			// Test URL redirection after the Replacement was created
			expect($location.path()).toBe('/replacements/' + sampleReplacementResponse._id);
		}));

		it('$scope.update() should update a valid Replacement', inject(function(Replacements) {
			// Define a sample Replacement put data
			var sampleReplacementPutData = new Replacements({
				_id: '525cf20451979dea2c000001',
				name: 'New Replacement'
			});

			// Mock Replacement in scope
			scope.replacement = sampleReplacementPutData;

			// Set PUT response
			$httpBackend.expectPUT(/replacements\/([0-9a-fA-F]{24})$/).respond();

			// Run controller functionality
			scope.update();
			$httpBackend.flush();

			// Test URL location to new object
			expect($location.path()).toBe('/replacements/' + sampleReplacementPutData._id);
		}));

		it('$scope.remove() should send a DELETE request with a valid replacementId and remove the Replacement from the scope', inject(function(Replacements) {
			// Create new Replacement object
			var sampleReplacement = new Replacements({
				_id: '525a8422f6d0f87f0e407a33'
			});

			// Create new Replacements array and include the Replacement
			scope.replacements = [sampleReplacement];

			// Set expected DELETE response
			$httpBackend.expectDELETE(/replacements\/([0-9a-fA-F]{24})$/).respond(204);

			// Run controller functionality
			scope.remove(sampleReplacement);
			$httpBackend.flush();

			// Test array after successful delete
			expect(scope.replacements.length).toBe(0);
		}));
	});
}());