'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Replacement = mongoose.model('Replacement'),
	_ = require('lodash');

/**
 * Create a Replacement
 */
exports.create = function(req, res) {
	var replacement = new Replacement(req.body);
	replacement.user = req.user;

	replacement.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(replacement);
		}
	});
};

/**
 * Show the current Replacement
 */
exports.read = function(req, res) {
	res.jsonp(req.replacement);
};

/**
 * Update a Replacement
 */
exports.update = function(req, res) {
	var replacement = req.replacement ;

	replacement = _.extend(replacement , req.body);

	replacement.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(replacement);
		}
	});
};

/**
 * Delete an Replacement
 */
exports.delete = function(req, res) {
	var replacement = req.replacement ;

	replacement.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(replacement);
		}
	});
};

/**
 * List of Replacements
 */
exports.list = function(req, res) { 
	Replacement.find().sort('-created').populate('user', 'displayName').exec(function(err, replacements) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(replacements);
		}
	});
};

/**
 * Replacement middleware
 */
exports.replacementByID = function(req, res, next, id) { 
	Replacement.findById(id).populate('user', 'displayName').exec(function(err, replacement) {
		if (err) return next(err);
		if (! replacement) return next(new Error('Failed to load Replacement ' + id));
		req.replacement = replacement ;
		next();
	});
};

/**
 * Replacement authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (req.replacement.user.id !== req.user.id) {
		return res.status(403).send('User is not authorized');
	}
	next();
};
