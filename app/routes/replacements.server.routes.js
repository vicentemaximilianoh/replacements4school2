'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var replacements = require('../../app/controllers/replacements.server.controller');

	// Replacements Routes
	app.route('/replacements')
		.get(replacements.list)
		.post(users.requiresLogin, replacements.create);

	app.route('/replacements/:replacementId')
		.get(replacements.read)
		.put(users.requiresLogin, replacements.hasAuthorization, replacements.update)
		.delete(users.requiresLogin, replacements.hasAuthorization, replacements.delete);

	// Finish by binding the Replacement middleware
	app.param('replacementId', replacements.replacementByID);
};
