'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Replacement Schema
 */
var ReplacementSchema = new Schema({
	name: {
		type: String,
		default: '',
		required: 'Please fill Replacement name',
		trim: true
	},
	created: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

mongoose.model('Replacement', ReplacementSchema);