'use strict';

var should = require('should'),
	request = require('supertest'),
	app = require('../../server'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	Replacement = mongoose.model('Replacement'),
	agent = request.agent(app);

/**
 * Globals
 */
var credentials, user, replacement;

/**
 * Replacement routes tests
 */
describe('Replacement CRUD tests', function() {
	beforeEach(function(done) {
		// Create user credentials
		credentials = {
			username: 'username',
			password: 'password'
		};

		// Create a new user
		user = new User({
			firstName: 'Full',
			lastName: 'Name',
			displayName: 'Full Name',
			email: 'test@test.com',
			username: credentials.username,
			password: credentials.password,
			provider: 'local'
		});

		// Save a user to the test db and create new Replacement
		user.save(function() {
			replacement = {
				name: 'Replacement Name'
			};

			done();
		});
	});

	it('should be able to save Replacement instance if logged in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Replacement
				agent.post('/replacements')
					.send(replacement)
					.expect(200)
					.end(function(replacementSaveErr, replacementSaveRes) {
						// Handle Replacement save error
						if (replacementSaveErr) done(replacementSaveErr);

						// Get a list of Replacements
						agent.get('/replacements')
							.end(function(replacementsGetErr, replacementsGetRes) {
								// Handle Replacement save error
								if (replacementsGetErr) done(replacementsGetErr);

								// Get Replacements list
								var replacements = replacementsGetRes.body;

								// Set assertions
								(replacements[0].user._id).should.equal(userId);
								(replacements[0].name).should.match('Replacement Name');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to save Replacement instance if not logged in', function(done) {
		agent.post('/replacements')
			.send(replacement)
			.expect(401)
			.end(function(replacementSaveErr, replacementSaveRes) {
				// Call the assertion callback
				done(replacementSaveErr);
			});
	});

	it('should not be able to save Replacement instance if no name is provided', function(done) {
		// Invalidate name field
		replacement.name = '';

		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Replacement
				agent.post('/replacements')
					.send(replacement)
					.expect(400)
					.end(function(replacementSaveErr, replacementSaveRes) {
						// Set message assertion
						(replacementSaveRes.body.message).should.match('Please fill Replacement name');
						
						// Handle Replacement save error
						done(replacementSaveErr);
					});
			});
	});

	it('should be able to update Replacement instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Replacement
				agent.post('/replacements')
					.send(replacement)
					.expect(200)
					.end(function(replacementSaveErr, replacementSaveRes) {
						// Handle Replacement save error
						if (replacementSaveErr) done(replacementSaveErr);

						// Update Replacement name
						replacement.name = 'WHY YOU GOTTA BE SO MEAN?';

						// Update existing Replacement
						agent.put('/replacements/' + replacementSaveRes.body._id)
							.send(replacement)
							.expect(200)
							.end(function(replacementUpdateErr, replacementUpdateRes) {
								// Handle Replacement update error
								if (replacementUpdateErr) done(replacementUpdateErr);

								// Set assertions
								(replacementUpdateRes.body._id).should.equal(replacementSaveRes.body._id);
								(replacementUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should be able to get a list of Replacements if not signed in', function(done) {
		// Create new Replacement model instance
		var replacementObj = new Replacement(replacement);

		// Save the Replacement
		replacementObj.save(function() {
			// Request Replacements
			request(app).get('/replacements')
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Array.with.lengthOf(1);

					// Call the assertion callback
					done();
				});

		});
	});


	it('should be able to get a single Replacement if not signed in', function(done) {
		// Create new Replacement model instance
		var replacementObj = new Replacement(replacement);

		// Save the Replacement
		replacementObj.save(function() {
			request(app).get('/replacements/' + replacementObj._id)
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Object.with.property('name', replacement.name);

					// Call the assertion callback
					done();
				});
		});
	});

	it('should be able to delete Replacement instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Replacement
				agent.post('/replacements')
					.send(replacement)
					.expect(200)
					.end(function(replacementSaveErr, replacementSaveRes) {
						// Handle Replacement save error
						if (replacementSaveErr) done(replacementSaveErr);

						// Delete existing Replacement
						agent.delete('/replacements/' + replacementSaveRes.body._id)
							.send(replacement)
							.expect(200)
							.end(function(replacementDeleteErr, replacementDeleteRes) {
								// Handle Replacement error error
								if (replacementDeleteErr) done(replacementDeleteErr);

								// Set assertions
								(replacementDeleteRes.body._id).should.equal(replacementSaveRes.body._id);

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to delete Replacement instance if not signed in', function(done) {
		// Set Replacement user 
		replacement.user = user;

		// Create new Replacement model instance
		var replacementObj = new Replacement(replacement);

		// Save the Replacement
		replacementObj.save(function() {
			// Try deleting Replacement
			request(app).delete('/replacements/' + replacementObj._id)
			.expect(401)
			.end(function(replacementDeleteErr, replacementDeleteRes) {
				// Set message assertion
				(replacementDeleteRes.body.message).should.match('User is not logged in');

				// Handle Replacement error error
				done(replacementDeleteErr);
			});

		});
	});

	afterEach(function(done) {
		User.remove().exec();
		Replacement.remove().exec();
		done();
	});
});